
-- TUT003 : OCTO579 : Test of simple UPDATE queries in composite database (multiple primary key columns)

-- Test UPDATE where 1 non primary key column is modified.

UPDATE composite SET name = 'Name11' where name = 'Name9';
SELECT * FROM composite;
UPDATE composite SET name = name || 'new1' where id1 > 1;
SELECT * FROM composite;
UPDATE composite SET name = name || 'new2' where id7 = 8;
SELECT * FROM composite;
UPDATE composite SET name = name || 'new3';
SELECT * FROM composite;

-- Test OR operator in WHERE clause works (tests DNF plan expansion logic)
-- Note: A similar query exists in "tests/fixtures/TUT010.sql". But it is here mostly to verify
-- that DNF plan expansion did happen whereas it is there in the TUT010.sql to verify correctness
-- of Octo output against Postgres.
UPDATE composite SET name = name || '?' where name = 'Name4' OR id4 = 5;
SELECT * FROM composite;

UPDATE 1
ID0|ID1|ID2|ID3|ID4|ID5|ID6|ID7|NAME
0|1|2|3|4|5|6|7|Name1
0|1|2|3|4|5|6|8|Name2
0|1|2|3|4|5|7|7|Name3
0|1|2|3|4|5|8|7|Name4
0|1|2|3|4|6|8|7|Name5
0|1|2|3|5|6|8|7|Name6
0|1|2|4|5|6|8|7|Name7
0|1|3|4|5|6|8|7|Name8
0|2|3|4|5|6|8|7|Name11
1|2|3|4|5|6|8|7|Name10
(10 rows)
UPDATE 2
ID0|ID1|ID2|ID3|ID4|ID5|ID6|ID7|NAME
0|1|2|3|4|5|6|7|Name1
0|1|2|3|4|5|6|8|Name2
0|1|2|3|4|5|7|7|Name3
0|1|2|3|4|5|8|7|Name4
0|1|2|3|4|6|8|7|Name5
0|1|2|3|5|6|8|7|Name6
0|1|2|4|5|6|8|7|Name7
0|1|3|4|5|6|8|7|Name8
0|2|3|4|5|6|8|7|Name11new1
1|2|3|4|5|6|8|7|Name10new1
(10 rows)
UPDATE 1
ID0|ID1|ID2|ID3|ID4|ID5|ID6|ID7|NAME
0|1|2|3|4|5|6|7|Name1
0|1|2|3|4|5|6|8|Name2new2
0|1|2|3|4|5|7|7|Name3
0|1|2|3|4|5|8|7|Name4
0|1|2|3|4|6|8|7|Name5
0|1|2|3|5|6|8|7|Name6
0|1|2|4|5|6|8|7|Name7
0|1|3|4|5|6|8|7|Name8
0|2|3|4|5|6|8|7|Name11new1
1|2|3|4|5|6|8|7|Name10new1
(10 rows)
UPDATE 10
ID0|ID1|ID2|ID3|ID4|ID5|ID6|ID7|NAME
0|1|2|3|4|5|6|7|Name1new3
0|1|2|3|4|5|6|8|Name2new2new3
0|1|2|3|4|5|7|7|Name3new3
0|1|2|3|4|5|8|7|Name4new3
0|1|2|3|4|6|8|7|Name5new3
0|1|2|3|5|6|8|7|Name6new3
0|1|2|4|5|6|8|7|Name7new3
0|1|3|4|5|6|8|7|Name8new3
0|2|3|4|5|6|8|7|Name11new1new3
1|2|3|4|5|6|8|7|Name10new1new3
(10 rows)
UPDATE 5
ID0|ID1|ID2|ID3|ID4|ID5|ID6|ID7|NAME
0|1|2|3|4|5|6|7|Name1new3
0|1|2|3|4|5|6|8|Name2new2new3
0|1|2|3|4|5|7|7|Name3new3
0|1|2|3|4|5|8|7|Name4new3
0|1|2|3|4|6|8|7|Name5new3
0|1|2|3|5|6|8|7|Name6new3?
0|1|2|4|5|6|8|7|Name7new3?
0|1|3|4|5|6|8|7|Name8new3?
0|2|3|4|5|6|8|7|Name11new1new3?
1|2|3|4|5|6|8|7|Name10new1new3?
(10 rows)
# Include key parts of generated M plans in reference file
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:;  UPDATE composite SET name = name || '?' where name = 'Name4' OR id4 = 5;
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:octoPlan0(cursorId,wrapInTp)
_ydboctoP*.m:octoPlan1(cursorId)
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"RowCount")=0 ; Initialize count of updated records
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME")=%ydboctoexpr IF '$ZYISSQLNULL(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME")) IF $DATA(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"))) DO
_ydboctoP*.m:    . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=""
_ydboctoP*.m:    . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")="")  DO
_ydboctoP*.m:    . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=""
_ydboctoP*.m:    . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")="")  DO
_ydboctoP*.m:    . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=""
_ydboctoP*.m:    . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")="")  DO
_ydboctoP*.m:    . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=""
_ydboctoP*.m:    . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")="")  DO
_ydboctoP*.m:    . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=""
_ydboctoP*.m:    . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")="")  DO
_ydboctoP*.m:    . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=""
_ydboctoP*.m:    . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")="")  DO
_ydboctoP*.m:    . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=""
_ydboctoP*.m:    . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")="")  DO
_ydboctoP*.m:    . . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=""
_ydboctoP*.m:    . . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")="")  DO
_ydboctoP*.m:    . . . . . . . . . . . IF $INCREMENT(%ydboctocursor(cursorId,"RowCount")) ; Increment count of updated records
_ydboctoP*.m:octoPlan2(cursorId)
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=""
_ydboctoP*.m:    FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")="")  DO
_ydboctoP*.m:    . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=""
_ydboctoP*.m:    . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")="")  DO
_ydboctoP*.m:    . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=""
_ydboctoP*.m:    . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")="")  DO
_ydboctoP*.m:    . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=""
_ydboctoP*.m:    . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")="")  DO
_ydboctoP*.m:    . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=%ydboctoexpr IF '$ZYISSQLNULL(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")) IF $DATA(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"))) DO
_ydboctoP*.m:    . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=""
_ydboctoP*.m:    . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")="")  DO
_ydboctoP*.m:    . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=""
_ydboctoP*.m:    . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")="")  DO
_ydboctoP*.m:    . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=""
_ydboctoP*.m:    . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")="")  DO
_ydboctoP*.m:    . . . . . . . . . . IF $INCREMENT(%ydboctocursor(cursorId,"RowCount")) ; Increment count of updated records
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:;  UPDATE composite SET name = name || 'new3';
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:octoPlan0(cursorId,wrapInTp)
_ydboctoP*.m:octoPlan1(cursorId)
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"RowCount")=0 ; Initialize count of updated records
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=""
_ydboctoP*.m:    FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")="")  DO
_ydboctoP*.m:    . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=""
_ydboctoP*.m:    . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")="")  DO
_ydboctoP*.m:    . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=""
_ydboctoP*.m:    . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")="")  DO
_ydboctoP*.m:    . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=""
_ydboctoP*.m:    . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")="")  DO
_ydboctoP*.m:    . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=""
_ydboctoP*.m:    . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")="")  DO
_ydboctoP*.m:    . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=""
_ydboctoP*.m:    . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")="")  DO
_ydboctoP*.m:    . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=""
_ydboctoP*.m:    . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")="")  DO
_ydboctoP*.m:    . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=""
_ydboctoP*.m:    . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")="")  DO
_ydboctoP*.m:    . . . . . . . . . IF $INCREMENT(%ydboctocursor(cursorId,"RowCount")) ; Increment count of updated records
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:;  UPDATE composite SET name = name || 'new1' where id1 > 1;
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:octoPlan0(cursorId,wrapInTp)
_ydboctoP*.m:octoPlan1(cursorId)
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"RowCount")=0 ; Initialize count of updated records
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=""
_ydboctoP*.m:    FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")="")  DO
_ydboctoP*.m:    . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=""
_ydboctoP*.m:    . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")="")  DO
_ydboctoP*.m:    . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=""
_ydboctoP*.m:    . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")="")  DO
_ydboctoP*.m:    . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=""
_ydboctoP*.m:    . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")="")  DO
_ydboctoP*.m:    . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=""
_ydboctoP*.m:    . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")="")  DO
_ydboctoP*.m:    . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=""
_ydboctoP*.m:    . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")="")  DO
_ydboctoP*.m:    . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=""
_ydboctoP*.m:    . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")="")  DO
_ydboctoP*.m:    . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=""
_ydboctoP*.m:    . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")="")  DO
_ydboctoP*.m:    . . . . . . . . . IF $INCREMENT(%ydboctocursor(cursorId,"RowCount")) ; Increment count of updated records
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:;  SELECT * FROM composite;
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:octoPlan0(cursorId,wrapInTp)
_ydboctoP*.m:octoPlan1(cursorId)
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=""
_ydboctoP*.m:    FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")="")  DO
_ydboctoP*.m:    . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=""
_ydboctoP*.m:    . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")="")  DO
_ydboctoP*.m:    . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=""
_ydboctoP*.m:    . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")="")  DO
_ydboctoP*.m:    . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=""
_ydboctoP*.m:    . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")="")  DO
_ydboctoP*.m:    . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=""
_ydboctoP*.m:    . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")="")  DO
_ydboctoP*.m:    . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=""
_ydboctoP*.m:    . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")="")  DO
_ydboctoP*.m:    . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=""
_ydboctoP*.m:    . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")="")  DO
_ydboctoP*.m:    . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=""
_ydboctoP*.m:    . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")="")  DO
_ydboctoP*.m:    . . . . . . . . . SET %ydboctocursor(cursorId,"keys",3,"","",%ydboctocursor(cursorId,"keys",3,"",""))=%ydboctoexpr
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:;  UPDATE composite SET name = 'Name11' where name = 'Name9';
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:octoPlan0(cursorId,wrapInTp)
_ydboctoP*.m:octoPlan1(cursorId)
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"RowCount")=0 ; Initialize count of updated records
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME")=%ydboctoexpr IF '$ZYISSQLNULL(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME")) IF $DATA(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"))) DO
_ydboctoP*.m:    . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=""
_ydboctoP*.m:    . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")="")  DO
_ydboctoP*.m:    . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=""
_ydboctoP*.m:    . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")="")  DO
_ydboctoP*.m:    . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=""
_ydboctoP*.m:    . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")="")  DO
_ydboctoP*.m:    . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=""
_ydboctoP*.m:    . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")="")  DO
_ydboctoP*.m:    . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=""
_ydboctoP*.m:    . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")="")  DO
_ydboctoP*.m:    . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=""
_ydboctoP*.m:    . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")="")  DO
_ydboctoP*.m:    . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=""
_ydboctoP*.m:    . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")="")  DO
_ydboctoP*.m:    . . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=""
_ydboctoP*.m:    . . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=$ORDER(^%ydboctoxref("COMPOSITE","NAME",%ydboctocursor(cursorId,"keys",1,"COMPOSITE","NAME"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")="")  DO
_ydboctoP*.m:    . . . . . . . . . . IF $INCREMENT(%ydboctocursor(cursorId,"RowCount")) ; Increment count of updated records
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:;  UPDATE composite SET name = name || 'new2' where id7 = 8;
_ydboctoP*.m:;; ---------------------------------------------------------
_ydboctoP*.m:octoPlan0(cursorId,wrapInTp)
_ydboctoP*.m:octoPlan1(cursorId)
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"RowCount")=0 ; Initialize count of updated records
_ydboctoP*.m:    SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=""
_ydboctoP*.m:    FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0")="")  DO
_ydboctoP*.m:    . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=""
_ydboctoP*.m:    . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1")="")  DO
_ydboctoP*.m:    . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=""
_ydboctoP*.m:    . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2")="")  DO
_ydboctoP*.m:    . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=""
_ydboctoP*.m:    . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3")="")  DO
_ydboctoP*.m:    . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=""
_ydboctoP*.m:    . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4")="")  DO
_ydboctoP*.m:    . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=""
_ydboctoP*.m:    . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5")="")  DO
_ydboctoP*.m:    . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=""
_ydboctoP*.m:    . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"))) QUIT:(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6")="")  DO
_ydboctoP*.m:    . . . . . . . SET %ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")=%ydboctoexpr IF '$ZYISSQLNULL(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7")) IF $DATA(^composite(%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID6"),%ydboctocursor(cursorId,"keys",1,"COMPOSITE","ID7"))) DO
_ydboctoP*.m:    . . . . . . . . . IF $INCREMENT(%ydboctocursor(cursorId,"RowCount")) ; Increment count of updated records
_ydboctoX*.m:;; ---------------------------------------------------------
_ydboctoX*.m:;; Generated M code maintains cross reference for NAME column in COMPOSITE table
_ydboctoX*.m:;; ---------------------------------------------------------
_ydboctoX*.m:    SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0")=""
_ydboctoX*.m:    FOR  SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0"))) QUIT:(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0")="")  DO
_ydboctoX*.m:    . SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1")=""
_ydboctoX*.m:    . FOR  SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1"))) QUIT:(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1")="")  DO
_ydboctoX*.m:    . . SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2")=""
_ydboctoX*.m:    . . FOR  SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2"))) QUIT:(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2")="")  DO
_ydboctoX*.m:    . . . SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID3")=""
_ydboctoX*.m:    . . . FOR  SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID3")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID3"))) QUIT:(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID3")="")  DO
_ydboctoX*.m:    . . . . SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID4")=""
_ydboctoX*.m:    . . . . FOR  SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID4")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID4"))) QUIT:(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID4")="")  DO
_ydboctoX*.m:    . . . . . SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID5")=""
_ydboctoX*.m:    . . . . . FOR  SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID5")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID5"))) QUIT:(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID5")="")  DO
_ydboctoX*.m:    . . . . . . SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID6")=""
_ydboctoX*.m:    . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID6")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID6"))) QUIT:(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID6")="")  DO
_ydboctoX*.m:    . . . . . . . SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID7")=""
_ydboctoX*.m:    . . . . . . . FOR  SET %ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID7")=$ORDER(^composite(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID0"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID1"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID2"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID3"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID4"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID5"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID6"),%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID7"))) QUIT:(%ydboctocursor(cursorId,"keys",2,"COMPOSITE","ID7")="")  DO
