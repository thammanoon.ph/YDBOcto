#################################################################
#								#
# Copyright (c) 2020-2021 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
}

@test "HP01 : Can load data into Postgres database/tables and communicate with the PostgreSQL server" {
	# Delete various temporary tables from Postgres in case they were created by prior test runs
	# and those tests failed before they could delete those temporary tables.
	# Not deleting these could cause this test to fail due to presence of additional tables that
	# this reference file is not aware of. But it is possible the database does not exist at all
	# so check for that first.
	if postgres_database_exists "names"; then
		psql names -c 'DROP TABLE IF EXISTS TDFT01;' >& postgres.drop_table_TDFT01.out
		psql names -c 'DROP TABLE IF EXISTS TDFT08;' >& postgres.drop_table_TDFT08.out
		psql names -c 'DROP TABLE IF EXISTS TUT002; DROP TABLE IF EXISTS TUT002B' >& postgres.drop_table_TUT002.out
		psql names -c 'DROP TABLE IF EXISTS TUT007' >& postgres.drop_table_TUT007.out
		psql names -c 'DROP TABLE IF EXISTS TUT008' >& postgres.drop_table_TUT008.out
		psql names -c 'DROP TABLE IF EXISTS TUT009;' >& postgres.drop_table_TUT009.out
		psql names -c 'drop table if exists TTA005A; drop table if exists TTA005B;' >& postgres.drop_table_TTA005.out
	fi
	if postgres_database_exists "composite"; then
		psql composite -c 'DROP TABLE IF EXISTS TUT010' >& postgres.drop_table_TUT010.out
	fi
	echo "\d" > listdb.sql
	for schema in names names1col customers pastas easynames northwind sqllogic{1,2,3,4,5} boolean nullnames nullcharnames composite
	do
		# If the database and/or table already exists, the `create_postgres_database` or `lost_postgres_fixture` function
		# invocations might return with a non-zero exit status due to the database being created by a different userid
		# (implying current userid does not have permissions to run CREATE DATABASE) OR due to `\set ON_ERROR_STOP on` usage
		# in the .sql script used by the `load_postgres_fixture` function etc.. Do not consider these as real errors hence
		# the use of the `run` command below to avoid bats from treating this as an error.
		create_postgres_database $schema &> $schema.psql.create || true
		load_postgres_fixture $schema postgres-$schema.sql || true
		# Do minimal check of database contents after Postgres load
		# First find list of table names in this database/schema
		psql $schema -f listdb.sql | grep public | awk '{print $3}' >& tables_$schema.txt
		declare -a tablearray
		readarray -t tablearray < tables_$schema.txt
		i=0
		while (( ${#tablearray[@]} > i )); do
			tablename=${tablearray[i++]}
			inputfile="input_${schema}_${tablename}.sql"
			outputfile="output_${schema}_${tablename}.txt"
			echo "select * from $tablename;" > $inputfile
			psql --no-align $schema -f $inputfile >& $outputfile
			echo "#######################################################################" >> output.txt
			echo "# Listing first 3 lines and last 3 lines of [SCHEMA : $schema] [TABLE : $tablename]" >> output.txt
			echo "#######################################################################" >> output.txt
			head -4 $outputfile >> output.txt 2>&1
			echo "..." >> output.txt
			echo "..." >> output.txt
			tail -4 $outputfile >> output.txt 2>&1
		done
	done
	export disable_escape_sequence_sanitize=1
	verify_output HP01 output.txt
}
